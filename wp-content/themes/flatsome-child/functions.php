<?php
// Add custom Theme Functions here

function ws_scripts_and_styles() {
    wp_enqueue_style( 'bootstrap-style', get_stylesheet_directory_uri() . "/assets/bootstrap-3.3.7-dist/css/bootstrap.css", array(),time() );
    wp_enqueue_style( 'fontawsome-style', get_stylesheet_directory_uri() . "/assets/font-awesome-4.7.0/css/font-awesome.css", array(),time() );
    wp_enqueue_style( 'custom-1-style', get_stylesheet_directory_uri() . "/assets/css/custom-1.css", array(),time() );
    wp_enqueue_style( 'custom-2-style', get_stylesheet_directory_uri() . "/assets/css/custome-2.css", array(),time() );
    wp_enqueue_style( 'custom-3-style', get_stylesheet_directory_uri() . "/assets/css/custom-3.css", array(),time() );

    wp_enqueue_script('bootstrap-js', get_stylesheet_directory_uri() .'/assets/bootstrap-3.3.7-dist/js/bootstrap.min.js', array(), time(), true, true);
    wp_enqueue_script('customjs-js', get_stylesheet_directory_uri() .'/assets/js/customjs.js', array(), time(), true, true);
}
add_action( 'wp_enqueue_scripts', 'ws_scripts_and_styles' );


/**
 * Main functions file
 *
 * @package WordPress
 * @subpackage Shop Isle
 */
@ini_set( 'upload_max_size' , '64M' );
@ini_set( 'post_max_size', '64M');
@ini_set( 'max_execution_time', '300' );
$vendor_file = trailingslashit( get_template_directory() ) . 'vendor/autoload.php';
if ( is_readable( $vendor_file ) ) {
    require_once $vendor_file;
}

if ( ! defined( 'WPFORMS_SHAREASALE_ID' ) ) {
    define( 'WPFORMS_SHAREASALE_ID', '848264' );
}



/**
 * Note: Do not add any custom code here. Please use a child theme so that your customizations aren't lost during updates.
 * http://codex.wordpress.org/Child_Themes
 */
// Add Shortcode
function showsearch($form) {
    $form = '<form role="search" method="get" id="searchform_talent" class="searchform" action="' . home_url( '/' ) . '" >
    <div class="form-search-talent">
    <input type="text" class="inputsearchtalent" placeholder="SEARCH TALENT" value="' . get_search_query() . '" name="s" id="s" />
    <button type="submit" id="searchsubmit"/><i class="fa fa-search"></i></button>
    </div>
    </form>';

    return $form;
}
add_shortcode( 'showsearchtalent', 'showsearch' );

function showVideoBanner($video){
    $video = '<div class="hero-block block">
    <div class="hero-block__wrapper">
        <div class="hero-block__video inline-background-cover">
            <video id="hero-block__video" muted="true" autoplay="true" loop="true">
                <source type="video/mp4" src="http://kanditree.wsoftpro.com/wp-content/uploads/2018/11/videoplayback.mp4-00.01.19.000-00.02.10.375.mp4">
            </video>
        </div>
        <div class="hero-block__contents">
            <h1 class="hero-block__title"><?php the_field(\'title_video_block\'); ?></h1>
        </div>
    </div>
</div>';
    return $video;
}
add_shortcode( 'videohomepage', 'showVideoBanner' );

function video_events_page( $atts ) {

    // Attributes
    $atts = shortcode_atts(
        array(
            'src' => 'http://kanditree.wsoftpro.com/wp-content/uploads/2018/11/videoplayback.mp4-00.01.19.000-00.02.10.375.mp4',
            'width' => '',
            'height' => '',
        ),
        $atts,
        'video_embed'
    );

    // Return custom embed code
    return '<embed
             src="' . $atts['src'] . '"
             width="' . $atts['width'] . '"
             height="' . $atts['height'] . '"
             type="application/x-shockwave-flash"
             allowscriptaccess="always"
             allowfullscreen="true">';

}


//navigation
if( !function_exists( 'bootstrap_pagination' ) ) {
function bootstrap_pagination( $query=null ) {
	  	global $wp_query;
	  	$query = $query ? $query : $wp_query;
	  	$big = 999999999;

	  	$paginate = paginate_links( array(
		    	'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
		    	'type' => 'array',
		    	'total' => $query->max_num_pages,
		    	'format' => '?paged=%#%',
		    	'current' => max( 1, get_query_var('paged') ),
		    	'prev_text' => 'PREVIOUS',
		    	'next_text' => 'NEXT',
	    	)
	  	);

	  	if ($query->max_num_pages > 1) :
		?><div class="container pagenav" style="position:relative;">
			<ul class="pagination">
	  	<?php
	  			foreach ( $paginate as $page ) {
	    			echo '<li>' . $page . '</li>';
	  			}
	  	?>
			</ul>
		</div>
		<?php
	  	endif;
	}
}

//
add_shortcode( 'shortcode_modal_talent', 'shortcode_modal_talent' );

function shortcode_modal_talent(){
    ob_start();
    global $post;
    $images = acf_photo_gallery('gallery_member',$post->ID); ?>
    <div class="block-thumbnails-talent">
		<div class="talent-imgs heroSlider-fixed">
			<div class="slider responsive">
        <?php
        $id=0;
        foreach($images as $image){
            ?>
				<div>
					<span id="#Modaltalent<?php echo $id; ?>"><image class="thumbnails-talent-popup" src="<?php echo $image['full_image_url']; ?>"></span>
					<div id="Modaltalent<?php echo $id; ?>" class="mymodalfade">
                    <!-- Modal content-->
                    <div class="modal-talent-content">
                        <button type="button">X</button>
                        <div class="modal-body">
                            <img class="modal-content" src="<?php echo $image['full_image_url']; ?>">
                        </div>
                    </div>
                	</div>
				</div>

            <?php $id++; } ?>
		</div>
		<div class="prev">
          <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
        </div>
        <div class="next">
          <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
        </div>
		</div>
    </div>
    <?php
    $list_post = ob_get_contents();
    ob_end_clean();
    return $list_post;
}


//
add_shortcode( 'shortcode_modal_talent_demo', 'shortcode_modal_talent_demo' );

function shortcode_modal_talent_demo(){
    ob_start();
    global $post;
    $images = acf_photo_gallery('gallery_member',$post->ID); ?>

		<div class="portfolio-slides">
        <?php
        $id=0;
        foreach($images as $image){
            ?>
			<div class="single">
				<a href="<?php echo $image['full_image_url']; ?>">
					<p style="background-image:url('<?php echo $image['full_image_url']; ?>')"></p>
				</a>
			</div>
            <?php } ?>
		</div>

		<script>
			jQuery(document).ready(function ($) {
				$('.portfolio-slides').slick({
				  infinite        : true,
				  slidesToShow    : 3,
				  slidesToScroll  : 1,
				  mobileFirst     : true
				});

				$('.portfolio-slides').slickLightbox({
				  itemSelector        : 'a',
				  navigateByKeyboard  : true
				});
			})
		</script>
    <?php
    $list_post = ob_get_contents();
    ob_end_clean();
    return $list_post;
}


//
add_shortcode( 'shortcode_customsearch_posttype', 'shortcode_customsearch_posttype' );

    function shortcode_customsearch_posttype(){
		ob_start();?>
			<form role="search" method="get" class="search-form" id="searchform" action="<?php echo get_permalink(); ?>">
				<label>
					<span class="screen-reader-text">Search for:</span>
					<input type="text" name="search" id="search" class="search-field" placeholder="Search …">
				</label>
				<input type="submit" class="search-submit" value="Search" />
			</form>

			<div class="search-result-mem">
        <?php
		if( isset( $_REQUEST['search'] ) ){
			$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
			$args = array(
				'paged'           => $paged,
				'posts_per_page'  => 6, //or any number
				'post_type'       => 'team', //or your custom post type if needed
				's'               => $_REQUEST[ 'search' ]
			);
		}
		if(isset($_GET['search']) && ($_GET['search']!="")){ ?><h3>Search result for "<?php echo $_GET['search']; ?>"</h3> <?php }
        $query = new WP_Query( $args );
        if( $query->have_posts() ){
			while( $query->have_posts() ){
                $query->the_post();  ?>
			<div class="team-member col-md-6 col-sm-6 col-xs-12">
				<div class="single-team-area">
					<a title="<?php the_title(); ?>" href="<?php the_permalink(); ?>">
					<img src="<?php echo get_the_post_thumbnail_url(); ?>" alt="<?php the_title(); ?>">
					</a>
					<div class="content-layout-talent">
						<div class="tlp-content">
							<h3 class="name">
								<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
							</h3>
						</div>
						<div class="short-bio"><p><?php the_field('short_bio'); ?></p></div>
						<div class="link-per"><a href="<?php the_permalink(); ?>">More info<i class="fa fa-arrow-right"></i></a></div>
					</div>

				</div>
			</div>

		<?php	}
        } ?>
		</div>
		<?php
        wp_reset_postdata();
		bootstrap_pagination($query);
        $list_post = ob_get_contents();
        ob_end_clean();
        return $list_post;
    }

//shortcode get product
add_shortcode( 'shortcode_get_ticket', 'shortcode_get_ticket' );

function shortcode_get_ticket(){
    ob_start();
	$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
    $args = array( 'post_type' => 'product', 'posts_per_page' => 6, 'paged' => $paged);
	$loop = new WP_Query( $args );
	?>
	<div class="product-page-custom" style="overflow:hidden">
<?php
	while ( $loop->have_posts() ) : $loop->the_post();
		global $product; ?>
		<div class="col-md-4 col-sm-12 per-ticket"><a class="hov-to-tran" href="<?php the_permalink(); ?>">
		<?php	echo woocommerce_get_product_thumbnail(); ?>
			<div class="backside-ticket" style="background-image:url('<?php the_field('backside_ticket',get_the_ID()); ?>')"></div></a><br />
			<a class="title-ticket-pg" href="<?php the_permalink(); ?>"><h3><?php the_title(); ?></h3></a>
			<p class="price-ticket-pg"><?php echo $product->get_price_html(); ?></p>
			<?php
		echo '<a class="a-book-tick" href="'.get_permalink().'">Order now</a>';
		echo '</div>';
	endwhile; ?>
	</div>
	<?php
  	wp_reset_query();
	bootstrap_pagination($loop);
    $list_post = ob_get_contents();
    ob_end_clean();
    return $list_post;
}



add_shortcode( 'shortcode_get_blog', 'shortcode_get_blog' );

function shortcode_get_blog(){
    ob_start();
	$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
    $args = array( 'category_name' => 'blog-page', 'posts_per_page' => 5, 'paged' => $paged);
	$loop = new WP_Query( $args );
	?>
	<div class="blog-page-custom" style="overflow:hidden">
<?php
	if( $loop->have_posts()){ $check=1;
	while ( $loop->have_posts() ) : $loop->the_post(); ?>

			<div class="per-blog">
				<a href="<?php the_permalink(); ?>">
					<div class="bgblog" style="background-image:url('<?php echo get_the_post_thumbnail_url();?>')">
						<div class="box-text-blog">
							<p><?php echo get_the_date(); ?></p>
							<h3><?php the_title(); ?></h3>
							<h4><?php _e('Read the Story'); ?></h4>
						</div>
					</div>
				</a>
			</div>

						<?php	 if($check == 2 ){ ?>
		<div class="blog-grid-item-spe hide-mobile">
			<a href="/submit-blog">
				<div class="blog-item-type-submit">
					<div class="fix-border">
						<p class="cont"><?php _e('Send us your blog'); ?></p>
						<h4 class="go-to-page-sharetip">Submit</h4>
						<i class="fa fa-arrow-right"></i>
					</div>
				</div>
			</a>
		</div>
		<?php }
	$check++;
	endwhile; }?>
	</div>
	<?php
  	wp_reset_query();
	bootstrap_pagination($loop);
    $list_post = ob_get_contents();
    ob_end_clean();
    return $list_post;
}

//shortcode get social media
function shortcode_social_media(){
    ob_start();
    $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
    $args = array( 'category_name' => 'social-media', 'posts_per_page' => 5, 'paged' => $paged);
	$loop = new WP_Query( $args );
    if( $loop->have_posts()){ ?>
		<div class="social-media-pg">
	<?php
	while ( $loop->have_posts() ) : $loop->the_post(); ?>
		<div class="per-social">
			<h2><?php the_title(); ?></h2>
        <?php
		global $post;
    	$images = acf_photo_gallery('gallery_member',$post->ID);
        foreach($images as $image){
            ?>
			<div class="featured-imgs-social col-md-4 col-sm-12">
				<img src="<?php echo $image['full_image_url']; ?>">
			</div>
            <?php } ?>
			<div class="featured-imgs-social col-md-4 col-sm-12">
				<?php the_content(); ?>
			</div>

			<a href="/contact"><?php _e('ENQUIRE'); ?></a>
		</div>
<?php
	endwhile; ?>
	</div>
	<?php	}
  	wp_reset_query();
	bootstrap_pagination($loop);
    $list_post = ob_get_contents();
    ob_end_clean();
    return $list_post;
}
add_shortcode( 'shortcode_social_media', 'shortcode_social_media' );


//get rl_gallery by cate
add_shortcode( 'shortcode_rl', 'shortcode_get_rl_gallery_by_category' );

function shortcode_get_rl_gallery_by_category($cat){
    ob_start();
    $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
    $args = array( 'post_type' => 'rl_gallery', 'rl_category' => $cat['slug_cate'], 'posts_per_page' => 6, 'paged' => $paged );
    $loop = new WP_Query( $args );
        if( $loop->have_posts()){ ?>
		<div class="row row-rl-gallery-page">
			<div class="rl-gallery-page">
		  		<?php   while ( $loop->have_posts() ) : $loop->the_post(); ?>
				<div class="col-md-6 col-sm-12">
					 <a href="<?php the_permalink(); ?>" >
						<div class="per-rl-gallery" style="background-image:url('<?php echo get_the_post_thumbnail_url(); ?>')">
							<h3>
								<?php the_title(); ?>
							</h3>
						 </div>
					</a>
				</div>
				<?php
					endwhile; ?>
			</div>
		</div>
		<?php }
    wp_reset_query();
    bootstrap_pagination($loop);
    $list_post = ob_get_contents();
    ob_end_clean();
    return $list_post;
}

//get rl_gallery by cate with content
add_shortcode( 'shortcode_rl_with_content', 'shortcode_rl_with_content' );

function shortcode_rl_with_content($cat){
    ob_start();
    $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
    $args = array( 'post_type' => 'rl_gallery', 'rl_category' => $cat['slug_cate'], 'posts_per_page' => $cat['posts_per_page'], 'paged' => $paged );
    $loop = new WP_Query( $args );
        if( $loop->have_posts()){ $i=1; ?>
		<div class="row row-rl-gallery-page-gal">
			<div class="rl-gallery-page-gal">
		  		<?php   while ( $loop->have_posts() ) : $loop->the_post(); ?>
				<div class="col-md-4 col-sm-12">
					 <a href="<?php the_permalink(); ?>" >
						<div class="per-rl-gallery-pg" style="background-image:url('<?php echo get_the_post_thumbnail_url(); ?>')">
						 </div>
						 <div class="box-text-per-gallerypage">
						 	<h3>
								<?php the_title(); ?>
							</h3>
							<p><?php the_field('description'); ?></p>
						 </div>
					</a>
				</div>
				<?php
					if($i%3==0){ echo '<div class="clearfix"></div>'; }
					$i++;
					endwhile; ?>
			</div>
		</div>
		<?php }
    wp_reset_query();
    bootstrap_pagination($loop);
    $list_post = ob_get_contents();
    ob_end_clean();
    return $list_post;
}

//
add_shortcode( 'shortcode_events_lightbox', 'shortcode_events_lightbox' );

function shortcode_events_lightbox($cat){
    ob_start();
    $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
    $args = array( 'category_name' => 'events_lightbox', 'posts_per_page' => $cat['posts_per_page'], 'paged' => $paged );
    $loop = new WP_Query( $args );
        if( $loop->have_posts()){ ?>
		<div class="row row-rl-gallery-page">
			<div class="rl-gallery-page spe">
		  		<?php   while ( $loop->have_posts() ) : $loop->the_post(); ?>
				<div class="col-md-6 col-sm-12">
					<a href="<?php the_permalink(); ?>">
						<div class="per-rl-gallery" style="background-image:url('<?php echo get_the_post_thumbnail_url(); ?>')">
							<h3><?php the_title(); ?></h3>
						</div>
					</a>
				</div>
				<?php
					endwhile; ?>
			</div>
		</div>
		<?php }
    wp_reset_query();
    bootstrap_pagination($loop);
    $list_post = ob_get_contents();
    ob_end_clean();
    return $list_post;
}

//
add_shortcode( 'shortcode_pr_marketing', 'shortcode_pr_marketing' );

function shortcode_pr_marketing(){
    ob_start();
    $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
    $args = array( 'category_name' => 'pr_marketing', 'posts_per_page' => 6, 'paged' => $paged );
    $loop = new WP_Query( $args );
	global $post;
        if( $loop->have_posts()){ ?>
		<div class="row row-pr-marketing-page">
			<?php   while ( $loop->have_posts() ) : $loop->the_post(); ?>
			<div class="row">
				<div class="col-md-6 col-sm-12">
					<div class="per-rl-gallery" style="background-image:url('<?php echo get_the_post_thumbnail_url(); ?>')">
					</div>
					<a href="<?php the_field('src_pr',$post->ID); ?>" target="_blank"><?php the_field('title_pr',$post->ID); ?></a>
				</div>
			</div>
			<?php endwhile; ?>
		</div>
		<?php }
    wp_reset_query();
    bootstrap_pagination($loop);
    $list_post = ob_get_contents();
    ob_end_clean();
    return $list_post;
}

add_action( 'woocommerce_after_cart', 'custom_after_cart' );
function custom_after_cart() {
    echo '<script>
    jQuery(document).ready(function($) {
        var upd_cart_btn = $(".update-cart-button");
        upd_cart_btn.hide();
        $(".cart-form").find(".qty").on("change", function(){
            upd_cart_btn.trigger("click");
        });
    });
    </script>';
}

//
add_filter( 'auto_update_plugin', '__return_false' );
//
add_filter( 'auto_update_theme', '__return_false' );